## [1.4.2](https://gitlab.gwdg.de/subugoe/textapi/specs/compare/v1.4.1...v1.4.2) (2024-01-22)


### Bug Fixes

* mime types, cdns ([d40b59b](https://gitlab.gwdg.de/subugoe/textapi/specs/commit/d40b59baad26ab1367be2abbca25163ad5efc223))

## [1.4.1](https://gitlab.gwdg.de/subugoe/textapi/specs/compare/v1.4.0...v1.4.1) (2024-01-22)


### Bug Fixes

* mime types, cdns ([a300815](https://gitlab.gwdg.de/subugoe/textapi/specs/commit/a3008159e03889f917e1a07d4f920aa04663a4cf))

# [1.4.0](https://gitlab.gwdg.de/subugoe/textapi/specs/compare/v1.3.1...v1.4.0) (2024-01-22)


### Bug Fixes

* :boom: adjust URL to new repository ([c5d885e](https://gitlab.gwdg.de/subugoe/textapi/specs/commit/c5d885e927f5d9d5b0883abc4c83c11762032504))
* adjust links to examples ([dda6b6d](https://gitlab.gwdg.de/subugoe/textapi/specs/commit/dda6b6dd0007b213161b38cb5a13c8b6189e5bf6))
* **ci:** set Gitlab access token ([1df024b](https://gitlab.gwdg.de/subugoe/textapi/specs/commit/1df024b7c03ce95646b1530b56d793eb2a02494e))
* hugo build for gitlab page and release page ([9d8586a](https://gitlab.gwdg.de/subugoe/textapi/specs/commit/9d8586a524a44005603626eb020b27085fbe81c8))
* semantic release target git repo ([3e1b904](https://gitlab.gwdg.de/subugoe/textapi/specs/commit/3e1b90494e1d87fc3c47aefe1d207d89d8eb9439))
* spelling ([596ceb6](https://gitlab.gwdg.de/subugoe/textapi/specs/commit/596ceb626f412389378a8e8de95f520014f5f437))
* spelling ([e25f012](https://gitlab.gwdg.de/subugoe/textapi/specs/commit/e25f0128d0dbf8a7804d1e4276f3b658cad6eb05))


### Features

* add docker build ([bdcca1d](https://gitlab.gwdg.de/subugoe/textapi/specs/commit/bdcca1d86129bceec477ea6cfd9560ab89047891))
* add docker build ([c171ccf](https://gitlab.gwdg.de/subugoe/textapi/specs/commit/c171ccfe2d0ebbdb00011c589bbeacb3ae2b3084))
* **ci:** container build on release only ([459a9ea](https://gitlab.gwdg.de/subugoe/textapi/specs/commit/459a9eaabf7fc4191a79a2265ff064babca73495))
* container ([85cda6e](https://gitlab.gwdg.de/subugoe/textapi/specs/commit/85cda6e81423ae8767f378188239dc86f216c1d2))

# [1.4.0](https://gitlab.gwdg.de/subugoe/textapi/specs/compare/v1.3.1...v1.4.0) (2024-01-22)


### Bug Fixes

* :boom: adjust URL to new repository ([c5d885e](https://gitlab.gwdg.de/subugoe/textapi/specs/commit/c5d885e927f5d9d5b0883abc4c83c11762032504))
* adjust links to examples ([dda6b6d](https://gitlab.gwdg.de/subugoe/textapi/specs/commit/dda6b6dd0007b213161b38cb5a13c8b6189e5bf6))
* **ci:** set Gitlab access token ([1df024b](https://gitlab.gwdg.de/subugoe/textapi/specs/commit/1df024b7c03ce95646b1530b56d793eb2a02494e))
* hugo build for gitlab page and release page ([9d8586a](https://gitlab.gwdg.de/subugoe/textapi/specs/commit/9d8586a524a44005603626eb020b27085fbe81c8))
* semantic release target git repo ([3e1b904](https://gitlab.gwdg.de/subugoe/textapi/specs/commit/3e1b90494e1d87fc3c47aefe1d207d89d8eb9439))
* spelling ([596ceb6](https://gitlab.gwdg.de/subugoe/textapi/specs/commit/596ceb626f412389378a8e8de95f520014f5f437))
* spelling ([e25f012](https://gitlab.gwdg.de/subugoe/textapi/specs/commit/e25f0128d0dbf8a7804d1e4276f3b658cad6eb05))


### Features

* add docker build ([bdcca1d](https://gitlab.gwdg.de/subugoe/textapi/specs/commit/bdcca1d86129bceec477ea6cfd9560ab89047891))
* add docker build ([c171ccf](https://gitlab.gwdg.de/subugoe/textapi/specs/commit/c171ccfe2d0ebbdb00011c589bbeacb3ae2b3084))
* **ci:** container build on release only ([459a9ea](https://gitlab.gwdg.de/subugoe/textapi/specs/commit/459a9eaabf7fc4191a79a2265ff064babca73495))
* container ([85cda6e](https://gitlab.gwdg.de/subugoe/textapi/specs/commit/85cda6e81423ae8767f378188239dc86f216c1d2))

# [1.4.0](https://gitlab.gwdg.de/subugoe/textapi/specs/compare/v1.3.1...v1.4.0) (2024-01-17)


### Bug Fixes

* :boom: adjust URL to new repository ([c5d885e](https://gitlab.gwdg.de/subugoe/textapi/specs/commit/c5d885e927f5d9d5b0883abc4c83c11762032504))
* adjust links to examples ([dda6b6d](https://gitlab.gwdg.de/subugoe/textapi/specs/commit/dda6b6dd0007b213161b38cb5a13c8b6189e5bf6))
* **ci:** set Gitlab access token ([1df024b](https://gitlab.gwdg.de/subugoe/textapi/specs/commit/1df024b7c03ce95646b1530b56d793eb2a02494e))
* hugo build for gitlab page and release page ([9d8586a](https://gitlab.gwdg.de/subugoe/textapi/specs/commit/9d8586a524a44005603626eb020b27085fbe81c8))
* semantic release target git repo ([3e1b904](https://gitlab.gwdg.de/subugoe/textapi/specs/commit/3e1b90494e1d87fc3c47aefe1d207d89d8eb9439))
* spelling ([596ceb6](https://gitlab.gwdg.de/subugoe/textapi/specs/commit/596ceb626f412389378a8e8de95f520014f5f437))
* spelling ([e25f012](https://gitlab.gwdg.de/subugoe/textapi/specs/commit/e25f0128d0dbf8a7804d1e4276f3b658cad6eb05))


### Features

* add docker build ([bdcca1d](https://gitlab.gwdg.de/subugoe/textapi/specs/commit/bdcca1d86129bceec477ea6cfd9560ab89047891))
* add docker build ([c171ccf](https://gitlab.gwdg.de/subugoe/textapi/specs/commit/c171ccfe2d0ebbdb00011c589bbeacb3ae2b3084))
* **ci:** container build on release only ([459a9ea](https://gitlab.gwdg.de/subugoe/textapi/specs/commit/459a9eaabf7fc4191a79a2265ff064babca73495))
* container ([85cda6e](https://gitlab.gwdg.de/subugoe/textapi/specs/commit/85cda6e81423ae8767f378188239dc86f216c1d2))

# [1.4.0](https://gitlab.gwdg.de/subugoe/textapi/specs/compare/v1.3.1...v1.4.0) (2024-01-15)


### Bug Fixes

* :boom: adjust URL to new repository ([c5d885e](https://gitlab.gwdg.de/subugoe/textapi/specs/commit/c5d885e927f5d9d5b0883abc4c83c11762032504))
* adjust links to examples ([dda6b6d](https://gitlab.gwdg.de/subugoe/textapi/specs/commit/dda6b6dd0007b213161b38cb5a13c8b6189e5bf6))
* **ci:** set Gitlab access token ([1df024b](https://gitlab.gwdg.de/subugoe/textapi/specs/commit/1df024b7c03ce95646b1530b56d793eb2a02494e))
* hugo build for gitlab page and release page ([9d8586a](https://gitlab.gwdg.de/subugoe/textapi/specs/commit/9d8586a524a44005603626eb020b27085fbe81c8))
* semantic release target git repo ([3e1b904](https://gitlab.gwdg.de/subugoe/textapi/specs/commit/3e1b90494e1d87fc3c47aefe1d207d89d8eb9439))
* spelling ([596ceb6](https://gitlab.gwdg.de/subugoe/textapi/specs/commit/596ceb626f412389378a8e8de95f520014f5f437))
* spelling ([e25f012](https://gitlab.gwdg.de/subugoe/textapi/specs/commit/e25f0128d0dbf8a7804d1e4276f3b658cad6eb05))


### Features

* add docker build ([bdcca1d](https://gitlab.gwdg.de/subugoe/textapi/specs/commit/bdcca1d86129bceec477ea6cfd9560ab89047891))
* add docker build ([c171ccf](https://gitlab.gwdg.de/subugoe/textapi/specs/commit/c171ccfe2d0ebbdb00011c589bbeacb3ae2b3084))
* **ci:** container build on release only ([459a9ea](https://gitlab.gwdg.de/subugoe/textapi/specs/commit/459a9eaabf7fc4191a79a2265ff064babca73495))
* container ([85cda6e](https://gitlab.gwdg.de/subugoe/textapi/specs/commit/85cda6e81423ae8767f378188239dc86f216c1d2))

# [1.4.0](https://gitlab.gwdg.de/subugoe/textapi/specs/compare/v1.3.1...v1.4.0) (2024-01-12)


### Bug Fixes

* :boom: adjust URL to new repository ([c5d885e](https://gitlab.gwdg.de/subugoe/textapi/specs/commit/c5d885e927f5d9d5b0883abc4c83c11762032504))
* adjust links to examples ([dda6b6d](https://gitlab.gwdg.de/subugoe/textapi/specs/commit/dda6b6dd0007b213161b38cb5a13c8b6189e5bf6))
* **ci:** set Gitlab access token ([1df024b](https://gitlab.gwdg.de/subugoe/textapi/specs/commit/1df024b7c03ce95646b1530b56d793eb2a02494e))
* hugo build for gitlab page and release page ([9d8586a](https://gitlab.gwdg.de/subugoe/textapi/specs/commit/9d8586a524a44005603626eb020b27085fbe81c8))
* semantic release target git repo ([3e1b904](https://gitlab.gwdg.de/subugoe/textapi/specs/commit/3e1b90494e1d87fc3c47aefe1d207d89d8eb9439))
* spelling ([596ceb6](https://gitlab.gwdg.de/subugoe/textapi/specs/commit/596ceb626f412389378a8e8de95f520014f5f437))
* spelling ([e25f012](https://gitlab.gwdg.de/subugoe/textapi/specs/commit/e25f0128d0dbf8a7804d1e4276f3b658cad6eb05))


### Features

* add docker build ([bdcca1d](https://gitlab.gwdg.de/subugoe/textapi/specs/commit/bdcca1d86129bceec477ea6cfd9560ab89047891))
* add docker build ([c171ccf](https://gitlab.gwdg.de/subugoe/textapi/specs/commit/c171ccfe2d0ebbdb00011c589bbeacb3ae2b3084))
* **ci:** container build on release only ([459a9ea](https://gitlab.gwdg.de/subugoe/textapi/specs/commit/459a9eaabf7fc4191a79a2265ff064babca73495))
* container ([85cda6e](https://gitlab.gwdg.de/subugoe/textapi/specs/commit/85cda6e81423ae8767f378188239dc86f216c1d2))

# [1.4.0](https://gitlab.gwdg.de/subugoe/textapi/specs/compare/v1.3.1...v1.4.0) (2023-07-06)

### Features

* **ci:** update workflow
* **ci:** add docker build

## [1.3.1](https://gitlab.gwdg.de/subugoe/emo/text-api/compare/v1.3.0...v1.3.1) (2023-03-20)


### Bug Fixes

* add `source` ([07fd5d8](https://gitlab.gwdg.de/subugoe/emo/text-api/commit/07fd5d8f2da6a04fd6c16de8308b0899668ba970))

# [1.3.0](https://gitlab.gwdg.de/subugoe/emo/text-api/compare/v1.2.0...v1.3.0) (2023-02-02)


### Bug Fixes

* add missing context ([f4f47ed](https://gitlab.gwdg.de/subugoe/emo/text-api/commit/f4f47ed733a1d8bdf1dbf06bdb859c698d014824))
* copy & paste error ([6013217](https://gitlab.gwdg.de/subugoe/emo/text-api/commit/6013217183ca2bea41e67ed6432533ae4ebbb35c))
* lang code `B` value unavailable for ISO 639-3 ([6889128](https://gitlab.gwdg.de/subugoe/emo/text-api/commit/6889128fbccbc949fdf4fd9a66ec3ec1c7f78760))
* page → item ([4245cfb](https://gitlab.gwdg.de/subugoe/emo/text-api/commit/4245cfb460b542d68ff00226cba4276b46cda7d4))
* required total field when paginated ([493beef](https://gitlab.gwdg.de/subugoe/emo/text-api/commit/493beef4b07c49b291f8ff1805e4e2278016dd01))
* validation ([49973f8](https://gitlab.gwdg.de/subugoe/emo/text-api/commit/49973f8906a0e769b07f022072117c8acef078cb))
* validation of Title Object ([195d563](https://gitlab.gwdg.de/subugoe/emo/text-api/commit/195d563eca64ddb5dd97889dc51847c1a0b186ff))


### Features

* add draft for pagination [#71](https://gitlab.gwdg.de/subugoe/emo/text-api/issues/71) ([5a99fed](https://gitlab.gwdg.de/subugoe/emo/text-api/commit/5a99fedd181ceaae4a314cf5d3704f0d4068eea6))
* add examples for AnnotationAPI end points ([0b05c85](https://gitlab.gwdg.de/subugoe/emo/text-api/commit/0b05c85288aef5e5745d4c5831524337515a2001))
* add ID to collection object ([761a707](https://gitlab.gwdg.de/subugoe/emo/text-api/commit/761a707086a5072aebf52971c1fea973751885c1))
* add link to documentation to release notes ([0bb6eb4](https://gitlab.gwdg.de/subugoe/emo/text-api/commit/0bb6eb4c93c8245924f85a3736904268df3758f8))
* add revision for annotationCollections for items ([86d301b](https://gitlab.gwdg.de/subugoe/emo/text-api/commit/86d301b222fb860abd7113270442fa691f12756d))
* add total field for paginated response ([78ac2b9](https://gitlab.gwdg.de/subugoe/emo/text-api/commit/78ac2b93e76ff9f566d71c2ade46acb81707ae99))
* add versions >= 1.1.0 to navigation ([e79ab3c](https://gitlab.gwdg.de/subugoe/emo/text-api/commit/e79ab3c46c7471b7cd51b03938afa11c9e8fe73d))
* adjust schema for new total field ([b46b0f9](https://gitlab.gwdg.de/subugoe/emo/text-api/commit/b46b0f9786429506f0dba0301fce121bdbc8dc1d))
* make collection optional in AnnotationAPI ([20104e1](https://gitlab.gwdg.de/subugoe/emo/text-api/commit/20104e19adcbbcf4810db6a9fd4fb0c0b6ab8d74))
* make collection optional; add examples ([c84b6a9](https://gitlab.gwdg.de/subugoe/emo/text-api/commit/c84b6a9c1e83cec9a28d00cca153be8de207218a))
* make ID required in ItemObject ([dbd5ca2](https://gitlab.gwdg.de/subugoe/emo/text-api/commit/dbd5ca2e48a6d08e50081b4d4dbe8e112175893b))
* remove old archiving mechanism ([ae6ed0a](https://gitlab.gwdg.de/subugoe/emo/text-api/commit/ae6ed0a9ab02c086eedab52919665ed35d728a4c))
* Validate sample server via CI + .editorconfig ([b210bde](https://gitlab.gwdg.de/subugoe/emo/text-api/commit/b210bde7b4ac4878ff506b7168c02f3c9384e7d0))

# [1.2.0](https://gitlab.gwdg.de/subugoe/emo/text-api/compare/v1.1.0...v1.2.0) (2022-11-03)


### Features

* add static fonts ([beaedc5](https://gitlab.gwdg.de/subugoe/emo/text-api/commit/beaedc51a9516779faf3bb888a3dec58ce6b5c0a))

# [1.1.0](https://gitlab.gwdg.de/subugoe/emo/text-api/compare/v1.0.0...v1.1.0) (2022-07-15)


### Bug Fixes

* add missing context property ([6b6bd93](https://gitlab.gwdg.de/subugoe/emo/text-api/commit/6b6bd93db20a284296d7135fb5cc8783130280df))


### Features

* add label property to sequence ([13246d1](https://gitlab.gwdg.de/subugoe/emo/text-api/commit/13246d1756201f9835cc5fd3f435a9c81ad88516))

# [1.0.0](https://gitlab.gwdg.de/subugoe/emo/text-api/compare/v0.9.0...v1.0.0) (2022-04-13)


### Bug Fixes

* dummy commit ([da896ae](https://gitlab.gwdg.de/subugoe/emo/text-api/commit/da896aef21e66270661afcd769d78a0ebc524ad9))


### BREAKING CHANGES

* Version 1.0.0 is ready for release!

# [0.9.0](https://gitlab.gwdg.de/subugoe/emo/text-api/compare/v0.8.1...v0.9.0) (2022-04-12)


### Bug Fixes

* links to sample JSONs ([fd1ae90](https://gitlab.gwdg.de/subugoe/emo/text-api/commit/fd1ae90f05029822d4dab7e04c7098268faa5454))


### Features

* add JSON-LD ([d780f7a](https://gitlab.gwdg.de/subugoe/emo/text-api/commit/d780f7aaef37bbd6de42f002f9a806b2dae731fd))

## [0.8.1](https://gitlab.gwdg.de/subugoe/emo/text-api/compare/v0.8.0...v0.8.1) (2022-04-12)


### Bug Fixes

* usage of Idref Object ([6a2c6b1](https://gitlab.gwdg.de/subugoe/emo/text-api/commit/6a2c6b1b8f1a194c4d14308c05e2aff866159165))

# [0.8.0](https://gitlab.gwdg.de/subugoe/emo/text-api/compare/v0.7.0...v0.8.0) (2022-04-08)


### Features

* add selectors to AnnotationAPI ([64c7ca8](https://gitlab.gwdg.de/subugoe/emo/text-api/commit/64c7ca876349eece86dd14f5e63cfe433274b1b3))

# [0.7.0](https://gitlab.gwdg.de/subugoe/emo/text-api/compare/v0.6.0...v0.7.0) (2022-04-08)


### Features

* add required main title ([28be42e](https://gitlab.gwdg.de/subugoe/emo/text-api/commit/28be42e1ffb480b86353a8a3cbe6995334700b74))

# [0.6.0](https://gitlab.gwdg.de/subugoe/emo/text-api/compare/v0.5.1...v0.6.0) (2022-04-06)


### Features

* implement sample server, adjust to newer Hugo versions ([fa30c34](https://gitlab.gwdg.de/subugoe/emo/text-api/commit/fa30c34077994b71094b70f61d8ff913a74b4c2e))

## [0.5.1](https://gitlab.gwdg.de/subugoe/emo/text-api/compare/v0.5.0...v0.5.1) (2022-04-05)


### Bug Fixes

* introduce Boolean where necessary ([5b2c736](https://gitlab.gwdg.de/subugoe/emo/text-api/commit/5b2c73613ee8e280978d190a2eddffaca7efb72e))

# [0.5.0](https://gitlab.gwdg.de/subugoe/emo/text-api/compare/v0.4.0...v0.5.0) (2022-04-04)


### Features

* add JSON Schema to specs ([2c4ea3f](https://gitlab.gwdg.de/subugoe/emo/text-api/commit/2c4ea3f84c0d5e11f3f5e0c93965d358d5e1246e))
* add OpenAPI ([05ce524](https://gitlab.gwdg.de/subugoe/emo/text-api/commit/05ce52403f6b1f6223acab0b375d2744675dcc31))

# [0.4.0](https://gitlab.gwdg.de/subugoe/emo/text-api/compare/v0.3.1...v0.4.0) (2022-04-01)


### Bug Fixes

* adjust document status to first release ([f2a36e1](https://gitlab.gwdg.de/subugoe/emo/text-api/commit/f2a36e170582281bb32f5af2377cb782c57eaee9))
* copypaste flaws ([7ce82b8](https://gitlab.gwdg.de/subugoe/emo/text-api/commit/7ce82b8828ed1e3dff5bc075653a50154b12600e))
* file ref ([29c5b10](https://gitlab.gwdg.de/subugoe/emo/text-api/commit/29c5b108f02c80f39b2808cc80308436c6e7c082))
* id base uri ([e5aa480](https://gitlab.gwdg.de/subugoe/emo/text-api/commit/e5aa4801e5206eae06d613fb8e4d1b8768d5b8d6))
* json schema version ([4cf2654](https://gitlab.gwdg.de/subugoe/emo/text-api/commit/4cf2654bf59c3325207e1bce682063b73f431c80))
* lang, format, typos ([e4f486d](https://gitlab.gwdg.de/subugoe/emo/text-api/commit/e4f486d267e834fce2102110c630330ae9a4670a))
* metadata recursion ([437775a](https://gitlab.gwdg.de/subugoe/emo/text-api/commit/437775a59715639ca2ce9f435d73d52e8cb2b689))
* module description ([2322e2a](https://gitlab.gwdg.de/subugoe/emo/text-api/commit/2322e2a1c2bbd02b77ce4392f1cd30092a49fb43))
* module description ([b697667](https://gitlab.gwdg.de/subugoe/emo/text-api/commit/b697667030af81b0e8b5c7613550f4cebe02350d))
* path to item in sample ([37645d2](https://gitlab.gwdg.de/subugoe/emo/text-api/commit/37645d208616cfd053ec11b5dd66e9c79fdb31ea))
* remove collector enfored role 'collector' ([52b05d5](https://gitlab.gwdg.de/subugoe/emo/text-api/commit/52b05d542a9dbc57ec579c03521da5a73a56872d))
* semver pattern ([85d8ea1](https://gitlab.gwdg.de/subugoe/emo/text-api/commit/85d8ea16d9f03d7e5fc7fab169431ed72147edea))
* update requried sections ([75e4669](https://gitlab.gwdg.de/subugoe/emo/text-api/commit/75e4669a3fcd9d89c9766a5ba810ba261af3e6fb))
* URL pattern ([a1bc421](https://gitlab.gwdg.de/subugoe/emo/text-api/commit/a1bc421185719af3b1768150dbeef86c89b0cd9c))
* version in sample data ([c411a20](https://gitlab.gwdg.de/subugoe/emo/text-api/commit/c411a20d64c1d2ec05e410e357997083dfd8cde1))


### Features

* add array types ([6f9deb4](https://gitlab.gwdg.de/subugoe/emo/text-api/commit/6f9deb4352c6732762c13463c6eeca3ca2c4df5d))
* add data integrity object to schema ([34fd644](https://gitlab.gwdg.de/subugoe/emo/text-api/commit/34fd644bb445b1761ddf002071e36b7a0d2429df))
* add first version of json schema ([806c98a](https://gitlab.gwdg.de/subugoe/emo/text-api/commit/806c98a4be0c72e8f41bdf81e7c60000b959ce50))
* add item to json schema ([70d99b8](https://gitlab.gwdg.de/subugoe/emo/text-api/commit/70d99b8b3a80f0f3dd1f1966c97089549704c909))
* add json schema ([45b4628](https://gitlab.gwdg.de/subugoe/emo/text-api/commit/45b4628d978fca759f526cbddc43021a21ba6f33))
* allow for xeno data, disallow any other ([8eb4676](https://gitlab.gwdg.de/subugoe/emo/text-api/commit/8eb4676495aeec5c314c35f53d82bb7c431dbf05))
* mime type pattern ([ada0079](https://gitlab.gwdg.de/subugoe/emo/text-api/commit/ada00795e21001b5a08ff770a504996c57de508a))
* recursive metadata object ([75334e4](https://gitlab.gwdg.de/subugoe/emo/text-api/commit/75334e49f2c32b15b0a44ccae0cd83f8ede3fade))
* restrict support mime types ([9ae3c09](https://gitlab.gwdg.de/subugoe/emo/text-api/commit/9ae3c09ffd59061f26d3ac27d8313294e92197dc))
* validation script ([28f33a5](https://gitlab.gwdg.de/subugoe/emo/text-api/commit/28f33a57f1b15e8225063b4ca5b0224020e91d48))

## [0.3.2](https://gitlab.gwdg.de/subugoe/emo/text-api/compare/v0.3.1...v0.3.2) (2022-03-18)


### Bug Fixes

* adjust document status to first release ([f2a36e1](https://gitlab.gwdg.de/subugoe/emo/text-api/commit/f2a36e170582281bb32f5af2377cb782c57eaee9))

## [0.3.1](https://gitlab.gwdg.de/subugoe/emo/text-api/compare/v0.3.0...v0.3.1) (2022-03-16)


### Bug Fixes

* add container to get a margin right ([bb9ad13](https://gitlab.gwdg.de/subugoe/emo/text-api/commit/bb9ad133ebe7e7d8eb5ff7380f2c19e00fb11da1))
* toc and menu styling ([cb8ff2f](https://gitlab.gwdg.de/subugoe/emo/text-api/commit/cb8ff2f4047f67aaa0e6ecb2486b0f312c353031))

# [0.3.0](https://gitlab.gwdg.de/subugoe/emo/text-api/compare/v0.2.1...v0.3.0) (2022-03-16)


### Bug Fixes

* move YUML definition to separate files ([d7da4e8](https://gitlab.gwdg.de/subugoe/emo/text-api/commit/d7da4e8aa22c1faac6c1dac1a86dfa4ee8624eaa))


### Features

* remove changelogs from specs ([471bc42](https://gitlab.gwdg.de/subugoe/emo/text-api/commit/471bc423671d744af138639bba4e3e705f289013))

## [0.2.1](https://gitlab.gwdg.de/subugoe/emo/text-api/compare/v0.2.0...v0.2.1) (2022-03-16)


### Bug Fixes

* copy&paste error ([48348bc](https://gitlab.gwdg.de/subugoe/emo/text-api/commit/48348bc1fc671ea038dd2fb5fd1330fef9eaac86))

# [0.2.0](https://gitlab.gwdg.de/subugoe/emo/text-api/compare/v0.1.0...v0.2.0) (2022-03-14)


### Bug Fixes

* add URL as data type ([c81ecb2](https://gitlab.gwdg.de/subugoe/emo/text-api/commit/c81ecb285e577fa8b2fad904b035fc4316b46cef))
* consider arrays ([3eb232a](https://gitlab.gwdg.de/subugoe/emo/text-api/commit/3eb232ad1586eb2eb0078c487a5ce187588a14cc))
* metadata object definition ([0fe84ee](https://gitlab.gwdg.de/subugoe/emo/text-api/commit/0fe84eecd8e3f906e24b88a23b6ec601bd70f550))
* Metadata Object definition ([e6be43d](https://gitlab.gwdg.de/subugoe/emo/text-api/commit/e6be43d7b4ec848201f3aa81595e067789de8671))
* move URL to URL ([23bb454](https://gitlab.gwdg.de/subugoe/emo/text-api/commit/23bb4549d8bff52375cc04085f28f2009b10b1f8))
* remove cardinality + ([c05f64c](https://gitlab.gwdg.de/subugoe/emo/text-api/commit/c05f64cc17454b8ce4df3a2005e6c16732b111b7))
* remove operator ([95e2762](https://gitlab.gwdg.de/subugoe/emo/text-api/commit/95e276235d0873f244ca76835971df336923e58c))
* typo ([242768d](https://gitlab.gwdg.de/subugoe/emo/text-api/commit/242768dc46f63bcdf01fa76b422dbede5b192797))
* typo ([f659dc2](https://gitlab.gwdg.de/subugoe/emo/text-api/commit/f659dc2d122ad34bda55b56846ee4fc6a680cddd))
* typo ([993547e](https://gitlab.gwdg.de/subugoe/emo/text-api/commit/993547eb46e4f8fd7fee66cc59283e3b8882a131))
* typo ([f66e299](https://gitlab.gwdg.de/subugoe/emo/text-api/commit/f66e2990c94b010f38e7d54dda5b64f57c640e61))
* typo ([0c81cd0](https://gitlab.gwdg.de/subugoe/emo/text-api/commit/0c81cd056d96a45b0bc973080267a5ffc3b7255b))
* update Body object and examples ([e8d485d](https://gitlab.gwdg.de/subugoe/emo/text-api/commit/e8d485d8107d585719f2065c826a25209d40998d))


### Features

* add AnnotationAPI ([82f545d](https://gitlab.gwdg.de/subugoe/emo/text-api/commit/82f545d12cf58a67d72da193e246d6bce2fd0df5))
* add checksums ([40df7df](https://gitlab.gwdg.de/subugoe/emo/text-api/commit/40df7dfe6c58394415839273c27959d8035c370c))
* add Content Object ([e3c1d31](https://gitlab.gwdg.de/subugoe/emo/text-api/commit/e3c1d31661ecc973649b5883f0f1e8eb8f9b57a2))
* add Content Object to Manifest ([e13520e](https://gitlab.gwdg.de/subugoe/emo/text-api/commit/e13520e5920e815fb4a45b264571cc04540fbb3d))
* add draft for license info ([7238738](https://gitlab.gwdg.de/subugoe/emo/text-api/commit/72387384039ecdd24ea6e8843b25c5b0a24181a6))
* add draft for listing modules in use ([87dd6e7](https://gitlab.gwdg.de/subugoe/emo/text-api/commit/87dd6e7d3b8444ffca81da030d00258c583a5e67))
* add ep module ([da0ff64](https://gitlab.gwdg.de/subugoe/emo/text-api/commit/da0ff64f30ce654a03a0cf510aa10962d2e79470))
* add FAQ ([b348d2e](https://gitlab.gwdg.de/subugoe/emo/text-api/commit/b348d2ebea451ded28072b27cde084d1f0cfcf8c))
* add first draft for editions module ([a79ab5b](https://gitlab.gwdg.de/subugoe/emo/text-api/commit/a79ab5b91ca1d93d321439b5ff48c1e73fdcbc44))
* add hierarchy for Metadata Objects ([a58e35a](https://gitlab.gwdg.de/subugoe/emo/text-api/commit/a58e35a24f968f206aec2ac2e28f11a01c804786))
* add items ([b37318d](https://gitlab.gwdg.de/subugoe/emo/text-api/commit/b37318d8f31f61dd5bc7882e0aabdd0667290de5))
* add manifests ([5d47fe1](https://gitlab.gwdg.de/subugoe/emo/text-api/commit/5d47fe1cf5b37da41823f766f30ee328717a8a6d))
* add mime type parameter description ([aa2e1e4](https://gitlab.gwdg.de/subugoe/emo/text-api/commit/aa2e1e425b8bbc209db94fff651c6294804a4bfb))
* add reusable integrity object ([1264057](https://gitlab.gwdg.de/subugoe/emo/text-api/commit/12640570e2bb6622579b3a16c9daf8d2488df8a2))
* add scope of module, specify descriptions ([4d51c5f](https://gitlab.gwdg.de/subugoe/emo/text-api/commit/4d51c5f9a249305c83a414ff5bda86824fff8281))
* add semantic versioning ([d0140b1](https://gitlab.gwdg.de/subugoe/emo/text-api/commit/d0140b19593f08917da0132e719893b68aa8168a))
* add specification for collection ([de8fde7](https://gitlab.gwdg.de/subugoe/emo/text-api/commit/de8fde70344689054d923d2ccdb199a67ae9bd33))
* allow for html in body value ([ec6b1ac](https://gitlab.gwdg.de/subugoe/emo/text-api/commit/ec6b1ac52cbe95942da066d1dbc4f0f26156753d))
* implement feedback ([d82342e](https://gitlab.gwdg.de/subugoe/emo/text-api/commit/d82342ef630da7b26d418b18794287ff66e4d09b))
* inital commit ([09a1669](https://gitlab.gwdg.de/subugoe/emo/text-api/commit/09a1669191e8dea831211af01ab1fac49ed70883))
* make  in Metadata Object optional ([4bf31ee](https://gitlab.gwdg.de/subugoe/emo/text-api/commit/4bf31ee83c31fb7bdf4d58fff24290cb6b79be40)), closes [#42](https://gitlab.gwdg.de/subugoe/emo/text-api/issues/42)
* make `total` and `startIndex` optional ([7cbf294](https://gitlab.gwdg.de/subugoe/emo/text-api/commit/7cbf294c08bf68715008f867855b3df481c41a78))
* make item title optional ([02e6162](https://gitlab.gwdg.de/subugoe/emo/text-api/commit/02e61625d6b8f81019bdec89a90db810bcea945f))
* replace URI with IRI ([77479c4](https://gitlab.gwdg.de/subugoe/emo/text-api/commit/77479c40c4928d01147b15b6f04a82d428471f97))
* update to ISO639-3 ([c0c3031](https://gitlab.gwdg.de/subugoe/emo/text-api/commit/c0c3031e70886713f3f24e2cd7f1c826c5a9aced))
