# TextAPI documentation

This repo holds the specification for the generic [TextAPI](https://textapi.sub.uni-goettingen.de) as GitLab page.
The TextAPI is a general API specification that is conceptionally based on the [IIIF Image API](https://iiif.io/api/image/3.0/).
It describes a set of JSON objects to be delivered by endpoints and consumed by [TIDO](https://gitlab.gwdg.de/subugoe/emo/tido).

## Getting the Pages Started Locally

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.
See deployment for notes on how to deploy the project on a live system.

### Prerequisites

* [`hugo`](https://gohugo.io/getting-started/installing/)

### Installing

#### Building locally

To work locally with this project, you'll have to follow the steps below:

1. Clone or download this project
2. Switch to the project's directory and type `hugo server` to preview the page
3. Add content
4. Generate the website: `hugo` (optional)

Read more at Hugo's [documentation](https://gohugo.io/overview/introduction/).

##### Preview your site

If you clone or download this project to your local computer and run `hugo server`,
your site can be accessed under `localhost:1313/hugo/`.

The theme used is adapted from <http://themes.gohugo.io/beautifulhugo/> and changed to work for Hugo > 0.92.0.

## Deployment

This project's static Pages are built by GitLab CI, following the steps defined in [`.gitlab-ci.yml`](.gitlab-ci.yml).
The page is built after each commit to the `main` branch and deployed to <https://textapi.sub.uni-goettingen.de>.

## Versioning

We use [SemVer](http://semver.org/) for versioning.
For the versions available, see the [tags on this repository](https://gitlab.gwdg.de/subugoe/textapi/specs/-/tags).

The versions are set automatically during a release via [semantic-release](https://www.npmjs.com/package/semantic-release).

## Authors

* **Mathias Göbel** - *Initial work*
* **Michelle Weidling**

See who is active and what has been going on recently in the [git graph](https://gitlab.gwdg.de/subugoe/textapi/specs/-/network/main?ref_type=heads).

## License

This project is licensed under the MIT License – see the [LICENSE](LICENSE) file for details.
The specs themselves are licensed under CC-BY-ND-4.0 – see the [LICENSE-specs](LICENSE-specs) file for details.
