#!/bin/bash

usage="$(basename) lastRelease.version nextRelease.gitHead
where:
    lastRelease.version  semantic version number of the last release
    nextRelease.gitHead  points to the git HEAD of the next release

Point to older versions on the hugo built website.
This script is meant to be used by semantic release and triggered as a
prepareCmd to prepare the release."

if [[ "$@" =~ "-help" ]]; then
  echo "$usage"
  exit 0
fi

if [ -z "$2" ]; then
  echo "Error: missing second argument."
  echo "$usage"
  exit 2
fi

TMP_DIR=$(mktemp -d)
cd $TMP_DIR || exit
eval $(ssh-agent -s)
echo "$DEPLOY_KEY" | tr -d '\r' | ssh-add - > /dev/null
mkdir -p ~/.ssh
chmod 700 ~/.ssh
ssh-keyscan gitlab.gwdg.de  >> ~/.ssh/known_hosts
git config --global user.email "$GIT_AUTHOR_EMAIL"
git config --global user.name "$GIT_AUTHOR_NAME"
git clone git@gitlab.gwdg.de:subugoe/textapi/specs.git
cd specs || exit

# add entry in menu
echo -n '\n[[menu.main]]\n' >> config.toml
echo "    name = '$1'"  >> config.toml
echo "    url = 'https://gitlab.gwdg.de/subugoe/textapi/specs/-/tree/$2/content/page'" >> config.toml
echo '    parent = "archive"' >> config.toml

# commit result
git status
git add --all
git commit -m "update versions"
git push
