{
  "$schema": "https://json-schema.org/draft/2020-12/schema",
  "$id": "item.json",
  "title": "TextAPI: Item",
  "description": "This is the main object to represent a single unit as part of a text, or the complete text.",
  "type": "object",
  "properties": {
    "@context": {
      "enum": ["https://gitlab.gwdg.de/subugoe/textapi/specs/-/raw/main/jsonld/item.jsonld"]
    },
    "textapi": {
      "description": "the TextAPI version covered by the implementation",
      "type": "string",
      "pattern": "^\\d+\\.\\d+\\.\\d+$"
    },
    "id": {
      "description": "URL pointing to this item",
      "type": "string",
      "pattern": "^(([^:/?#]+):)?(//([^/?#]*))?([^?#]*)(\\?([^#]*))?(#(.*))?"
    },
    "title": {
      "description": "the title of the item",
      "type": "array",
      "if": {
        "contains": {
          "required": [
            "type"
          ],
          "properties": {
            "type": {
              "const": "sub"
            }
          }
        }
      },
      "then": {
        "prefixItems": [
          {
            "$ref": "title-main.json"
          }
        ],
        "items": {
          "$ref": "title-sub.json"
        },
        "uniqueItems": true
      },
      "else": {
        "items": {
          "$ref": "title-main.json"
        },
        "minItems": 1,
        "maxItems": 1
      }

      },
    "type": {
      "type": "string",
      "enum": [ "section", "page", "full" ]
    },
    "n": {
      "description": "division number",
      "type": "string",
      "minLength": 1
    },
    "lang": {
      "description": "language codes according to iso639-3 describing the resource",
      "type": "array",
      "items": {
        "type": "string",
        "pattern": "[a-z]{3}"
      }
    },
    "langAlt": {
      "description": "alternative language name or code (when there is no iso639-3 code, e.g. karshuni)",
      "type": "array",
      "items": {
        "type": "string",
        "minItems": 1,
        "uniqueItems": true
      }
    },
    "content": {
      "description": "different serializations of the item, e.g. HTML, plain text, XML, …",
      "type": "array",
      "items": {
        "$ref": "content.json",
        "minItems": 1,
        "uniqueItems": true
      }
    },
    "description": {
      "description": "a short description of the object",
      "type": "string",
      "minLength": 1
    },
    "image": {
      "description": "corresponding image",
      "$ref": "image.json"
    },
    "annotationCollection": {
      "description": "URL pointing to an Annotation Collection for this item",
      "type": "string",
      "pattern": "^(([^:/?#]+):)?(//([^/?#]*))?([^?#]*)(\\?([^#]*))?(#(.*))?"
    },
    "modules": {
      "description": "the modules in use for this manifest. has to correspond to the Collection Object’s modules entry",
      "type": "array",
      "items": {
        "$ref": "module.json",
        "minItems": 1,
        "uniqueItems": true
      }
    }

  },
  "required": [ "@context", "textapi", "type", "lang", "content", "id" ],
  "patternProperties": {
    "^x-": {}
  },
  "additionalProperties": false
}