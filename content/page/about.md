---
title: TextAPI About
---

This specification aims to provide a robust way to interchange text resources
from different sources into a single viewer. We like to provide an interface to
our own resources (coming from a variety of projects, including those with a
status end-of-funding) and to provide a new interface in a corresponding viewer
that requires this specification.

The archetype is the [IIIF standard](https://iiif.io) that allows interoperability of
images from a wide range of repositories.

We are going to provide a JSON Schema and/or a validation engine as soon as
possible.

The API Design is licensed CC-BY-ND-4.0.
