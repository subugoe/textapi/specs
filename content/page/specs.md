---
title: TextAPI Specification
version: 1.4.0
---

## Preliminaries

The key words "MUST", "MUST NOT", "REQUIRED", "SHALL", "SHALL NOT", "SHOULD",
"SHOULD NOT", "RECOMMENDED", "NOT RECOMMENDED", "MAY", and "OPTIONAL" in this
document are to be interpreted as described in [BCP 14](https://tools.ietf.org/html/bcp14),
[RFC2119](https://tools.ietf.org/html/rfc2119) and
[RFC8174](https://tools.ietf.org/html/rfc8174) when, and only when, they appear
in all capitals, as shown here.

This document is licensed under [CC-BY-ND-4.0](https://creativecommons.org/licenses/by-nd/4.0/legalcode) ([via SPDX](https://spdx.org/licenses/CC-BY-ND-4.0.html)).

The style of this documentation is adapted from [OpenAPI 3.0.2](https://github.com/OAI/OpenAPI-Specification/blob/master/versions/3.0.2.md)
specification.

## Status of this Document

The current version: `1.4.0`.

## Definitions

### Cardinalities

| Cardinality | Description | REQUIRED |
|----|----|----|
| 1 | exactly one | yes |
| ? | zero or one | no |

### Data Types

| Type | Description |
|----|----|
| \[…\] | array |
| int | integer |
| string | a sequence of characters, MAY be empty |
| boolean | `true` or `false` |
| URL | a valid URL pointing to a resource |
| MIME type | a valid MIME type according to IANA. See [MDN](https://developer.mozilla.org/en-US/docs/Web/HTTP/Basics_of_HTTP/MIME_types) |
| semver | a string matching the pattern `^\d+\.\d+\.\d+$`, representing a semantic version number |
| iso639-3 | alpha-3 language code according to [ISO 639-3](https://iso639-3.sil.org/) |
| SPDX | a valid SPDX identifier (see <https://spdx.org/licenses/>) or – if not applicable, e.g. for copyrighted resources – `restricted` |

### URI Syntax

The URI Syntax is used according to the one described at [IIIF](https://iiif.io/api/image/2.1/#uri-syntax).

We encourage to allow for IRI (an internationalizes superset or URI) on client side.

## Delivery Service

The TextAPI delivery service is the description of endpoints.
It is REQUIRED to use the `https` protocol.

### Collection

Returns a [collection object](#collection-object).

`https://{server}{/prefix}/{collection}/collection.json`

#### Examples

* `https://example.com/textapi/Shakespeares_works/collection.json`
* `https://example.com/textapi/Shakespeares_works/collection.json?from=2&size=6`

#### Parameters

When a paginated result is given, the total number of items in the sequence MUST be
provided in the response.

| Parameter | Type | Description |
| --- | --- | --- |
| from | int | zero-based number of manifest to include in the sequence object |
| size | int | total of manifests to include in the sequence object |

### Manifest

Returns a [manifest object](#manifest-object).

`https://{server}{/prefix}{/collection?}/{manifest}/manifest.json`

#### Parameters

| Parameter | Type | Description |
| --- | --- | --- |
| from | int | zero-based number of items to include in the sequence object |
| size | int | total of items to include in the sequence object |

When the aforementioned parameters are not set, the default is to deliver the complete list.

#### Examples

* `https://example.com/textapi/Shakespeares_works/Othello/manifest.json`
* `https://example.com/textapi/Othello/manifest.json`
* `https://example.com/textapi/Othello/manifest.json?from=0&size=7`

### Item

Returns an [item object](#item-object).

`https://{server}{/prefix}/{collection}/{manifest}/{item}/{revision}/item.json`

`https://{server}{/prefix}{/collection?}/{manifest}/{item}/latest/item.json`

when a single item represents a complete text (whatever this means), the
corresponding endpoint is REQUIRED to be available at

`https://{server}{/prefix}/{collection}/{manifest}/{revision}/full.json`

#### Examples

* `https://example.com/textapi/Shakespeares_works/Othello/p2/rev2/item.json`
* `https://example.com/textapi/Othello/p2/rev2/item.json`
* `https://example.com/textapi/Shakespeares_works/Othello/p2/latest/item.json`
* `https://example.com/textapi/Othello/p2/latest/item.json`
* `https://example.com/textapi/Shakespeares_works/Othello/rev1/full.json`
* `https://example.com/textapi/Othello/rev1/full.json`

## Schema

All fields that are not explicitly REQUIRED or described with MUST or SHALL
are considered OPTIONAL.
It is RECOMMENDED, however, to provide as much information as possible.

### Collection Object

A collection contains a curated list of texts.
It is REQUIRED to be served at the corresponding [endpoint](#collection).

| Field Name | Cardinality | Type | Description |
|----|----|----|----|
| @context | 1 | URL | the JSON-LD context of this object. MUST be `https://gitlab.gwdg.de/subugoe/textapi/specs/-/raw/main/jsonld/collection.jsonld` |
| textapi | 1 | semver | the TextAPI version covered by the implementation |
| id | 1 | URL | URL pointing to this collection |
| title | 1 | \[[Title Object](#title-object)\] | the title of the collection. the main title `MUST` be the first one in the array |
| collector | 1 | \[[Actor Object](#actor-object)\] | a personal entity responsible for the collection (collector) |
| description | ? | string | description of the collection |
| sequence | 1 |\[[Sequence Object](#sequence-object)\] | a set of manifests included in this collection |
| total | ? | int | Number of items the sequence might provide. Required when response is paginated. |
| annotationCollection | ? | URL | URL pointing to an [Annotation Collection](https://www.w3.org/TR/annotation-model/#annotation-collection) for the complete collection |
| modules | ? | \[[Module Object](#module-object)\] | the modules in use for this collection |

#### Sequence Object

Represents a sequence of collections, manifests or items.
Within a manifest it SHOULD contain items exclusively.
An item that represents a complete manifest (i.e. that has the type `full`) MUST be the first item in the sequence.

A sequence might be separated into chunks on request (GET parameters `from` and `size`). Thus implementations has to maintain the order of items in this array.

| Field Name | Cardinality | Type | Description |
|----|----|----|----|
| @context | 1 | URL | the JSON-LD context of this object. MUST be `https://gitlab.gwdg.de/subugoe/textapi/specs/-/raw/main/jsonld/sequence.jsonld` |
| id | 1 | URL | URL to find a [Manifest Object](#manifest-object), [Collection Object](#collection-object) or [Item Object](#item-object) |
| type | 1 | string | one of `collection`, `manifest`, `item` |
| label | ? | string | human-readable name or title. `SHOULD` correspond to the collection's/manifest's/item's `label` or `title` property |

#### Module Object

Gives information about which [API modules](../modules) are in use.
If the Module Object is set, at least one modules has to be selected.

*Note: The list of modules is contiuously enhanced.*

| Field Name | Cardinality | Type | Description |
|----|----|----|----|
| edition-manuscripts | ? | boolean | `true` or `false` |
| edition-prints | ? | boolean | `true` or `false` |

### Manifest Object

This is the main object in the schema to represent a single text, its derivatives
(e.g. html) and therefore containing the metadata.
It is REQUIRED to be served at the corresponding [endpoint](#manifest).

| Field Name | Cardinality | Type | Description |
|----|----|----|----|
| @context | 1 | URL | the JSON-LD context of this object. MUST be `https://gitlab.gwdg.de/subugoe/textapi/specs/-/raw/main/jsonld/manifest.jsonld` |
| textapi | 1 | semver | version number satisfied by the implementation |
| id | 1 | URL | URL pointing to this manifest |
| label | 1 | string | human-readable name or title |
| sequence | 1 | \[[Sequence Object](#sequence-object)\] | a sequence of `items` |
| total | ? | int | Number of items the sequence might provide. Required when response is paginated. |
| actor | ? | \[[Actor Object](#actor-object)\] | a personal entity related to the document (e.g. author or editor) |
| repository | ? | \[[Repository Object](#repository-object)\] | a repository archiving the document(s) or source |
| image | ? | [Image Object](#image-object) | an image representing the resources (e.g. thumbnail or logo) |
| metadata | ? | \[[Metadata Object](#metadata-object)\] | further metadata |
| support | ? | \[[Support Object](#support-object)\] |
| license | 1 | \[[License Object](#license-object)\] | license under which the resource MUST be used |
| description | ? | string | a short description of the object |
| annotationCollection | ? | URL | URL pointing to an [Annotation Collection](https://www.w3.org/TR/annotation-model/#annotation-collection) for the complete manifest |
| modules | ? | \[[Module Object](#module-object)\] | the modules in use for this manifest. has to correspond to the Collection Object's `modules` entry |

#### Repository Object

A repository archiving the source or derivates, e.g. facsimiles or digitized
versions.

| Field Name | Cardinality | Type | Description |
|----|----|----|----|
| @context | 1 | URL | the JSON-LD context of this object. MUST be `https://gitlab.gwdg.de/subugoe/textapi/specs/-/raw/main/jsonld/repository.jsonld` |
| label | ? | string | the label as given by the hosting institution |
| url | 1 | URL | URL pointing to the website of the institution |
| baseUrl | 1 | URL | a base URL where `id` can be resolved |
| id | 1 | string | the identifier at the hosting institution |

#### Actor Object

| Field Name | Cardinality | Type | Description |
|----|----|----|----|
| @context | 1 | URL | the JSON-LD context of this object. MUST be `https://gitlab.gwdg.de/subugoe/textapi/specs/-/raw/main/jsonld/actor.jsonld` |
| role | 1 | [string] | the role of a personal entity in relation to the parent object, MUST be `collector` in case of collections |
| name | 1 | string | the principal name of the person |
| id | ? | string | internal identifier |
| idref | ? | \[[Idref Object](#idref-object)\] | authority files related to the person |

#### Metadata Object

A set of metadata describing the source or its context.
Mainly used for key-value pairs.

| Field Name | Cardinality | Type | Description |
|----|----|----|----|
| key | 1 | string | label |
| value | ? | string  | property. MUST be set if the object has no `metadata` field. MUST NOT be set if the object has a `metadata` field. |
| metadata | ? | \[[Metadata Object](#metadata-object)\] | further metadata that is subordinant to the current metadata entry. MUST be set if the object has no `value` field. MUST NOT be set if the object has a `value` field. |

#### Image Object

An image representing the source or its provider.
It is recommended that a [IIIF Image API](https://iiif.io/api/image/2.0/) service is available for this image for manipulations such as resizing.

| Field Name | Cardinality | Type | Description |
|----|----|----|----|
| @context | 1 | URL | the JSON-LD context of this object. MUST be `https://gitlab.gwdg.de/subugoe/textapi/specs/-/raw/main/jsonld/image.jsonld` |
| id | 1 | URL | URL pointing to the image |
| manifest | ? | URL | URL pointing to the image's manifest object |
| license | 1 | [License Object](#license-object) | the license for the image that MUST be used |

#### License Object

The license or any other appropriate rights statement the resources is served under.
It is REQUIRED to use one of [SPDX](https://spdx.org/licenses/).

| Field Name | Cardinality | Type | Description |
|----|----|----|----|
| id | 1 | SPDX | an SPDX identifier |
| notes | ? | string | further notes concerning the license. can be used e.g. for the attribution statement of CC BY. `restricted` License Objects `SHOULD` set the `notes` property. it is `RECOMMENDED` to adhere to the [format of copyright notices](https://reuse.software/spec/#format-of-copyright-notices) suggested by the [REUSE Specification](https://reuse.software/spec) |

#### Support Object

Any material supporting the view is described and referenced in this object.
This encompasses fonts and CSS, but also other material to support the rendering MAY
be added on request.

| Field Name | Cardinality | Type | Description |
|----|----|----|----|
| @context | 1 | URL | the JSON-LD context of this object. MUST be `https://gitlab.gwdg.de/subugoe/textapi/specs/-/raw/main/jsonld/support.jsonld` |
| type | 1 | one of `font`, `css` |
| mime | 1 | MIME type | the MIME type for the resource |
| url | 1 | URL | URL pointing to the resource |
| integrity | ? | [Data Integrity Object](#data-integrity-object) | information on data integrity |

### Item Object

It is REQUIRED to be served at the corresponding [endpoint](#item).

When an item serves a complete version of a text, the `type` SHOULD be `full`.
Any other (sliced) material is either a `section` or a `page`.

| Field Name | Cardinality | Type | Description |
|----|----|----|----|
| @context | 1 | URL | the JSON-LD context of this object. MUST be `https://gitlab.gwdg.de/subugoe/textapi/specs/-/raw/main/jsonld/item.jsonld` |
| textapi | 1 | semver | version number satisfied by the implementation |
| id | 1 | URL | URL pointing to this item |
| title | ? | \[[Title Object](#title-object)\] | the title of the item. the main title `MUST` be the first one in the array |
| type | 1 | string | one of `section`, `page`, `full` |
| n | ? | string | division number |
| lang | 1 | [iso639-3] | language codes describing the resource |
| langAlt | ? | [string] | alternative language name or code (when there is no iso639-3 code, e.g. `karshuni`) |
| content | 1 | \[[Content Object](#content-object)\] | different serializations of the item, e.g. HTML, plain text, XML, ... |
| description | ? | string | a short description of the object |
| image | ? | [Image Object](#image-object) | corresponding image |
| annotationCollection | ? | URL | URL pointing to an [Annotation Collection](https://www.w3.org/TR/annotation-model/#annotation-collection) for this item |
| modules | ? | \[[Module Object](#module-object)\] | the modules in use for this item. has to correspond to the Collection Object's `modules` entry |

#### Content Object

| Field Name | Cardinality | Type | Description |
|----|----|----|----|
| @context | 1 | URL | the JSON-LD context of this object. MUST be `https://gitlab.gwdg.de/subugoe/textapi/specs/-/raw/main/jsonld/content.jsonld` |
| url | 1 | URL | URL pointing to the content |
| type | 1 | MIME type | a MIME type. If several Content Objects with the same MIME type are provided, these SHOULD be distinguished with a [MIME type parameter](https://developer.mozilla.org/en-US/docs/Web/HTTP/Basics_of_HTTP/MIME_types#structure_of_a_mime_type) where the key is type and the value can be freely chosen, e.g. text/html;type=transcription. |
| integrity | ? | [Data Integrity Object](#data-integrity-object) | information on data integrity |

### Title Object

| Field Name | Cardinality | Type | Description |
|----|----|----|----|
| @context | 1 | URL | the JSON-LD context of this object. MUST be `https://gitlab.gwdg.de/subugoe/textapi/specs/-/raw/main/jsonld/title.jsonld` |
| title | 1 | string | a single title |
| type | 1 | string | one of `main`, `sub`. if several titles are provided, at least one of them MUST have the type `main` |

### Idref Object

| Field Name | Cardinality | Type | Description |
|----|----|----|----|
| @context | 1 | URL | the JSON-LD context of this object. MUST be `https://gitlab.gwdg.de/subugoe/textapi/specs/-/raw/main/jsonld/idref.jsonld` |
| base | ? | URL | the base URL to the authority file |
| type | 1 | string | short title of the referenced authority |
| id | 1 | string | the main ID referenced |

### Data Integrity Object

This object contains information regarding data integrity (checksum) of the referenced resources. It `SHOULD` be used for all resources.

| Field Name | Cardinality | Type | Description |
|----|----|----|----|
| type | 1 | string | algorithm to calculate checksum (e.g. `MD5`, `SHA-256`) |
| value | 1 | string | the checksum |

## Extensibility

All objects MAY be extended by own keywords, always prefixed by `x-`.

## Appendix

### Schema Files

An [OpenAPI](https://swagger.io/specification/) specification of the TextAPI can be found in the [API's repository](https://gitlab.gwdg.de/subugoe/textapi/specs/-/blob/main/openapi_3.0.2_specification.yml).

An [JSON Schema](https://json-schema.org/) specification of the TextAPI can be found in the [API's repository](https://gitlab.gwdg.de/subugoe/textapi/specs/-/tree/main/schema).

### Class Diagram

![UML class diagram](https://yuml.me/65910c03.jpg)

### Example Objects

You can find examples in our git repository:

* [this version's collection.json on git](https://gitlab.gwdg.de/subugoe/textapi/specs/-/blob/main/sample_server/12a34b/collection.json)
* [this version's manifest.json on git](https://gitlab.gwdg.de/subugoe/textapi/specs/-/blob/main/sample_server/12a34b/12a34b/manifest.json)
* [this version's item.json on git](https://gitlab.gwdg.de/subugoe/textapi/specs/-/blob/main/sample_server/12a34b/12a34b/3x_2/fb406b265a779b43afa635bc8da79d81/item.json)

## Todos

We are working to improve the TextAPI. Please find our todo list attached to the git repo [here](https://gitlab.gwdg.de/subugoe/textapi/specs/-/issues).
