---
title: Editions (Prints)
subtitle: Module
---

## Preliminaries

The key words "MUST", "MUST NOT", "REQUIRED", "SHALL", "SHALL NOT", "SHOULD",
"SHOULD NOT", "RECOMMENDED", "NOT RECOMMENDED", "MAY", and "OPTIONAL" in this
document are to be interpreted as described in [BCP 14](https://tools.ietf.org/html/bcp14),
[RFC2119](https://tools.ietf.org/html/rfc2119) and
[RFC8174](https://tools.ietf.org/html/rfc8174) when, and only when, they appear
in all capitals, as shown here.

This document is licensed under [CC-BY-ND-4.0](https://creativecommons.org/licenses/by-nd/4.0/legalcode) ([via SPDX](https://spdx.org/licenses/CC-BY-ND-4.0.html)).

The style of this documentation is adapted from [OpenAPI 3.0.2](https://github.com/OAI/OpenAPI-Specification/blob/master/versions/3.0.2.md)
specification.

## Status of this Document

The current version of this document is `0.0.1` according
to [Semantic Versioning 2.0.0](https://semver.org/spec/v2.0.0.html).

As long as the version number is not increased to `1.0.0` this document is
considered to be an early draft and major changes MAY be implemented over night and
without any announcement.

## Scope of this Module

This module serves metadata usually needed for editing printed editions.
Its metadata fields refer to the bibliographic information about the text edited, e.g. when and where it has been printed or which part of the printed text is covered.

## Definitions

### Cardinalities

| Cardinality | Description | REQUIRED |
|----|----|----|
| 1 | exactly one | yes |
| + | one or more | yes |
| ? | zero or one | no |
| * | zero or more | no |

### Data Types

| Type | Description |
|----|----|
| \[…\] | array |
| string | a sequence of characters, MAY be empty |

## Module Schema

If you decide to use this schema, make sure to set at least all required fields. Register the module in your main TextAPI in the [Module Object](/specs#modules-object).

This module claims the prefix `ep-`.

### Manifest Object

| Field Name | Cardinality | Type | Description |
|----|----|----|----|
| ep-section-title | ? | string | the title of the edited section as provided by the print's editor. SHOULD be set only if a part of a printed text is edited |
| ep-editor | 1 | \[[Actor Object](#actor-object)\] | the editor of the printed text |
| ep-title | 1 | \[[Title Object](#title-object)\] | the title of the printed text |
| ep-publisher | 1 | \[[Actor Object](#actor-object)\] | the publisher of the print. This typically refers to a firm or another kind of institution |
| ep-pubPlace | 1 | string | the publication place of the printed text |
| ep-date | 1 | string | the year of the print's publication |
| ep-biblScope | 1 | string | the pages the edited section covers in the printed text in the format `pp–pp` or `full` if the complete printed text is edited |
