---
title: FAQ
---

## Interplay of text and annotation panel

### Does the target ID of the AnnotationAPI have to have a specific format?

Generally, the ID provided in the AnnotationAPI's _target.id_ and the ID of the corresponding HTML element have to comply to the requirements of [HTML id attributes](https://developer.mozilla.org/en-US/docs/Web/HTML/Global_attributes/id).

If your source data is XML we recommend using XPath's `generate-id()` function to create such an ID.

### How do I establish the connection between the TextAPI and the AnnotationAPI so that TIDO is able to display it?

TIDO is designed to recognize the target of an annotation if the part after the last slash in a target's ID matches the ID of an element in the text panel.

Take this HTML element which is displayed in the text panel as an example:

```html
<span id="N.1.2.3.4.5" class="person">Johann Wolfang von Goethe</span>
```

To enable TIDO to detect an annotation referring to this element, the corresponding _target.id_ entry in the AnnotationAPI would have to look like this:

```json
{
"body": {
    "type": "TextualBody",
    "value": "Johann Wolfgang von Goethe is a famous German poet.",
    "format": "text/plain",
    "x-content-type": "Person"
},
"target": {
    "format": "text/xml",
    "language": "en",
    "id": "http://some-url.org/ns/annotations/N.1.2.3.4.5"
},
"type": "Annotation",
"id": "http://some-url.org/ns/annotations/annotation-N.1.2.3.4.5"
}
```

Anything before the last slash (in this case `http://some-url.org/ns/annotations/`) aren't considered for establishing the connection.

## Metadata

### How can I use the TextAPI to express that my item has RTL text?

Currently, the TextAPI provides no information on the language's directional flow (LTR/RTL).

To display your RTL text passages correctly in TIDO you can use a designated CSS class for the element in your HTML you want to have displayed in RTL:

```html
<div class="rtl">
    <span class="line">Some text</span>
    <span class="line">… and some more text</span>
</div>
```

Add the following rule to the CSS file you provide in the [Support Object](../specs/#support-object):

```css
.rtl {
    direction: rtl;
}
```

## Metadata visualization

### How can link lists expressed in TextAPI so that TIDO will display within the metadata panel?

Apart from the core metadata that is already provided by the TextAPI, the specs offer the more generic [Metadata Object](../specs/#metadata-object).

To achieve a list of URLs we recommend using this object recursively with the URL as `key` and the link text as `value` like so:

```json
"metadata": [
   {
       "key": "Further information",
       "value": "",
       "metadata": [
       {
           "key": "http://example.com/item1",
           "value": "A link description"
       },
       {
           "key": "http://example.com/item2",
           "value": "Another link description"
       }
       ]
   }
]
```

TIDO will render this structure as a list of URLs having the top `metadata`'s `key` as a list heading.

## License

The documents provided here are licensed under [CC-BY-ND-4.0](https://creativecommons.org/licenses/by-nd/4.0/legalcode) ([via SPDX](https://spdx.org/licenses/CC-BY-ND-4.0.html)). This encompasses the main Text API specifications as well as the Annotations Specification and the modules.
