---
title: Modules
---

When working with resources from different origins, having a standard that describes how certain things are done is most convenient.

The TextAPI has been designed to deliver text resources of various kinds in a standardized way and allows for customization where the API specifiation is not sufficient.
This approach bears both possibility and risk:
While it empowers projects to offer more details about their resources it can hugely decrease interoperability since every project is likely to define its own custom (metadata) keys.

This is where the TextAPI Modules come into play.

The TextAPI modules are sets of predefined metadata keys that projects typically want to provide for a certain type of text resource.

Opting for a module adds to the interoperability of the project APIs and reduces a project's need to forge their custom keys.
Nevertheless, a module's keys may be enhanced by further project specific customizations (preceding with the `x-` prefix).
Modules can also be combined if a text resource fits into more than one category.

## Usage

Register all used modules in the [Module Object](../specs#module-object)).

> **Caution**:
>
> If a module is chosen for providing additional fields, all cardinalities of the fields defined in the module's schema have to be met.
>
> Using e.g. only the fields that refer to a Collection Object while omitting the module's required fields for another Object is not possible.

## Available Modules

Currently the TextAPI provides the following modules:

- [Editions (Manuscripts)](../em)
- [Editions (Prints)](../ep)
