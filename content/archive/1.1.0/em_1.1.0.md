---
title: Editions (Manuscripts)
subtitle: Module
version: 1.1.0
---

## Preliminaries

The key words "MUST", "MUST NOT", "REQUIRED", "SHALL", "SHALL NOT", "SHOULD",
"SHOULD NOT", "RECOMMENDED", "NOT RECOMMENDED", "MAY", and "OPTIONAL" in this
document are to be interpreted as described in [BCP 14](https://tools.ietf.org/html/bcp14),
[RFC2119](https://tools.ietf.org/html/rfc2119) and
[RFC8174](https://tools.ietf.org/html/rfc8174) when, and only when, they appear
in all capitals, as shown here.

This document is licensed under [CC-BY-ND-4.0](https://creativecommons.org/licenses/by-nd/4.0/legalcode) ([via SPDX](https://spdx.org/licenses/CC-BY-ND-4.0.html)).

The style of this documentation is adapted from [OpenAPI 3.0.2](https://github.com/OAI/OpenAPI-Specification/blob/master/versions/3.0.2.md)
specification.

## Status of this Document

The current version of this document is `0.0.1` according
to [Semantic Versioning 2.0.0](https://semver.org/spec/v2.0.0.html).

As long as the version number is not increased to `1.0.0` this document is
considered to be an early draft and major changes MAY be implemented over night and
without any announcement.

## Definitions

### Cardinalities

| Cardinality | Description | REQUIRED |
|----|----|----|
| 1 | exactly one | yes |
| + | one or more | yes |
| ? | zero or one | no |
| * | zero or more | no |

### Data Types

| Type | Description |
|----|----|
| \[…\] | array |
| string | a sequence of characters, MAY be empty |
| URI | a valid URI, see [URI Syntax](#uri-syntax) |
| iso3166-2 | alpha-2 country code according to [ISO 3166 alpha-2](https://www.iso.org/iso-3166-country-codes.html) |

## Module Schema

If you decide to use this schema, make sure to set at least all required fields. Register the module in your main TextAPI in the [Module Object](/specs#modules-object).

This module claims the prefix `em-`.

### Collection Object

The following fields `MAY` be added to a [Collection Object](../specs_1.1.0/#collection-object) describing e.g. an edition comprising one or more manuscripts.

**Caution:** `em-editor` is required.

| Field Name | Cardinality | Type | Description |
|----|----|----|----|
| em-editor | + |  \[[Actor Object](../specs_1.1.0#actor-object)\] | a personal entity responsible for the edition (editor) |
| em-funder | * | \[[Actor Object](../specs_1.1.0#actor-object)\] | an entity responsible for funding the project in which the text(s) in focus is/are edited (funder) |

### Manifest Object

The following fields `MAY` be added to a [Manifest Object](../specs_1.1.0/#manifest-object) describing a manuscript or fragment.

**Caution:** `em-identifier` is required.

| Field Name | Cardinality | Type | Description |
|----|----|----|----|
| em-origin | ? | string | the origin of the manuscript. `MAY` also comprise full provenence information. |
| em-identifier | 1 | string | the identifier of the manuscript, e.g. a shelfmark |
| em-textClass | * | string | a term classifying the text's genre(s) or category/categories, preferably taken from a controlled vocabulary |

#### Repository Object

The following fields `MUST` be added to a [Repository Object](../specs_1.1.0/#repository-object) describing the current location of the manuscript:

| Field Name | Cardinality | Type | Description |
|----|----|----|----|
| em-repoCountry | 1 | iso3166-2 | the country in which the repository is located as ISO 3166 alpha-2 code, e.g. `DE` for Germany |
| em-repoPlace | 1 | string | the name of a settlement, e.g. city or village, in which the repository in focus is located |
