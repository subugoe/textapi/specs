---
title: AnnotationAPI Specification [1.1.0]
version: 1.1.0
---

## Preliminaries

The key words "MUST", "MUST NOT", "REQUIRED", "SHALL", "SHALL NOT", "SHOULD",
"SHOULD NOT", "RECOMMENDED", "NOT RECOMMENDED", "MAY", and "OPTIONAL" in this
document are to be interpreted as described in [BCP 14](https://tools.ietf.org/html/bcp14),
[RFC2119](https://tools.ietf.org/html/rfc2119) and
[RFC8174](https://tools.ietf.org/html/rfc8174) when, and only when, they appear
in all capitals, as shown here.

This document is licensed under [CC-BY-ND-4.0](https://creativecommons.org/licenses/by-nd/4.0/legalcode) ([via SPDX](https://spdx.org/licenses/CC-BY-ND-4.0.html)).

The style of this documentation is adapted from [OpenAPI 3.0.2](https://github.com/OAI/OpenAPI-Specification/blob/master/versions/3.0.2.md)
specification.

## Status of this Document

The current version of this document is `1.0.0`.

The version number will be applied according to [Semantic Versioning 2.0.0](https://semver.org/spec/v2.0.0.html).

## Definitions

### W3C Annotations

The AnnotationAPI is an implementation of the [W3C Annotation Model](https://www.w3.org/TR/annotation-model/).
This means that we provide an [Annotation Collection](https://www.w3.org/TR/annotation-model/#annotation-collection) per resource and [Annotation Pages](https://www.w3.org/TR/annotation-model/#annotation-page) for each Annotation Collection.

The W3C specs, however, don't assume hierarchical/nested resources as we have in the IIIF derived TextAPI model.

In order to bring these two approaches together, all Objects of the TextAPI (Collection/Manifest/Item) provide an endpoint for delivering an Annotation Collection.
These list the next level items (Collection → Manifest, Manifest → Item, Item → Item) as Annotation Pages including all annotations on that level.
Since Collection Objects are on the top of the hierarchy there aren't any Annotation Pages provided for them.

Additions to the W3C model are marked with an `x-`.

### Cardinalities

| Cardinality | Description | REQUIRED |
|----|----|----|
| 1 | exactly one | yes |
| ? | zero or one | no |
| * | zero or more | no |

### Data Types

| Type | Description |
|----|----|
| \[…\] | array |
| string | a sequence of characters, MAY be empty |
| IRI | a valid IRI, see [IRI Syntax](#iri-syntax) |
| MIME type | a valid MIME type according to IANA. See [MDN](https://developer.mozilla.org/en-US/docs/Web/HTTP/Basics_of_HTTP/MIME_types) |
| iso639-3 | alpha-3 language code according to [ISO 639-3](https://iso639-3.sil.org). In cases of alternative codes the usage of `bibliographic (B)` is REQUIRED. |
| int | a non-negative Integer value |

### IRI Syntax

The URI Syntax is used according to the one described at [IIIF](https://iiif.io/api/image/2.1/#uri-syntax).

We encourage to allow for IRI (an internationalizes superset or URI) on client side.

## Delivery Service / Available Endpoints

The AnnotationAPI delivery service is the description of endpoints.
All endpoints use the `https` protocol.

### Annotations for Collection Objects

`https://{server}{/prefix}/{collection}/annotationCollection.json`

Returns an [Annotation Collection](#annotation-collection) for the given collection.

### Annotations for Manifest Objects

`https://{server}{/prefix}/{collection}/{$manifest}/annotationCollection.json`

Returns an [Annotation Collection](#annotation-collection) for the given manifest.

---

`https://{server}{/prefix}/{collection}/{$manifest}/annotationPage.json`

Returns an [Annotation Page](#annotation-page) for the given manifest.

### Annotations for Item Objects

`https://{server}{/prefix}/{collection}/{$manifest}/{$page}/annotationCollection.json`

Returns an [Annotation Collection](#annotation-collection) for the given item.

---

`https://{server}{/prefix}/{collection}/{$manifest}/{$page}/annotationPage.json`

Returns an [Annotation Page](#annotation-page) for the given item.

## Schema

### Annotation Collection

An Annotation Collection presents an overview of the Annotation Pages provided for a given resource.

| Field Name | Cardinality | Type | Description |
|----|----|----|----|
| @context | 1 | IRI | MUST be `http://www.w3.org/ns/anno.jsonld` |
| id | 1 | IRI | a unique identifier for the Annotation Collection |
| type | 1 | string | MUST be `AnnotationCollection` |
| label | 1 | string | a descriptive title for the Annotation Collection |
| total | ? | int | the number of annotations belonging to this Collection |
| first | 1 | IRI | an IRI pointing to the API endpoint of the first Annotation Page of this Collection |
| last | 1 | IRI | an IRI pointing to the API endpoint of the last Annotation Page of this Collection. identical to `first` if the collection has only one Annotation Page |

### Annotation Page

An Annotation Page lists all annotations that occur on them while linking to the previous and next Annotation Pages within an Annotation Collection.

| Field Name | Cardinality | Type | Description |
|----|----|----|----|
| @context | 1 | IRI | MUST be `http://www.w3.org/ns/anno.jsonld` |
| id | 1 | IRI | a unique identifier for the Annotation Collection |
| type | 1  | string | MUST be `AnnotationPage` |
| partOf | 1 | \[[PartOf Object](#partof-object)\] | information about the Annotation Collection the pPage is part of |
| next | 1 | IRI | an IRI pointing to the API endpoint of the next Annotation Page of this Annotation Collection. MAY be `null` if there is no following Page |
| prev | 1 | IRI | an IRI pointing to the API endpoint of the previous Annotation Page of this Annotation Collection. MAY be `null` if there is no previous Page |
| startIndex | ? | int | a number denoting the position of this Page's first annotation, relative to the parent Annotation Collection |
| items | 1 | \[[Annotation Item Object](#annotation-item-object)\] | a sequence of annotations |

---

### PartOf Object

Represents a relation between an Annotation Page and an Annotation Collection.

Contains information about the Page's parent Annotation Collection.

| Field Name | Cardinality | Type | Description |
|----|----|----|----|
| id | 1 | IRI | a unique identifier for the Annotation Collection |
| label | 1 | string | a descriptive title for the Annotation Collection |
| total | ? | int | the number of annotations belonging to this Collection. |

### Annotation Item Object

A sequence of annotations belonging to an Annotation Page.

| Field Name | Cardinality | Type | Description |
|----|----|----|----|
| body | 1 | \[[Body Object](#body-object)\] | the content of the annotation |
| target | 1 | \[[Target Object](#target-object)\] | information about the target of the annotation |
| type | 1 | string | MUST be `Annotation`|
| id | 1 | IRI | a unique identifier for the annotation |

### Body Object

The content of the annotation.

| Field Name | Cardinality | Type | Description |
|----|----|----|----|
| type | 1 | string | MUST be `TextualBody` |
| value | 1 | string | the content of the annotation |
| format | 1 | MIME type | `text/plain` OR `text/html` (see notes) |
| x-content-type | 1 | string | the semantic type of the annotation. can be chosen freely by each project, e.g. `Person` or `Place` |

#### Notes

When using HTML within `value` the set of allowed elements is limited to the following:

- p
- span
- a

Besides the `href` attribute for `html:a` no further attribute SHOULD be used.

### Target Object

Provides information about the target resource the annotation refers to.

| Field Name | Cardinality | Type | Description |
|----|----|----|----|
| selector | 1 | [CssSelector](#selector-object-cssselector) or [Range Selector](#selector-object-range) | a selector pointing to the target |
| format | 1 | MIME type | the MIME type of the target resource |
| language | 1 | iso639-3 | the language of the target resource |

### Selector Object: CssSelector

Provides information about the target resource the annotation refers to.

| Field Name | Cardinality | Type | Description |
|----|----|----|----|
| type | 1 | string | `CssSelector` |
| value | 1 | string | a CSS selector in most cases referencing an ID or class. |

### Selector Object: RangeSelector

Provides information about the target resource the annotation refers to.

| Field Name | Cardinality | Type | Description |
|----|----|----|----|
| type | 1 | string | `RangeSelector` |
| startSelector | 1 | [Selector Object](#selector-object-cssselector) | `CssSelector` representing the start of the range. |
| endSelector | 1 | [Selector Object](#selector-object-cssselector) | `CssSelector` representing the end of the range. |

## Appendix

### Class Diagram

![UML class diagram](https://yuml.me/90d725ea.svg)

### Examples

#### Annotation Collection: Example for Collection

`https://www.example.com/collection/annotationCollection.json`

```json
{
  "annotationCollection" : {
    "total" : 52,
    "@context" : "http://www.w3.org/ns/anno.jsonld",
    "first" : "https://www.example.com/collection/manifest/annotationPage.json",
    "label" : "Annotations for some collection.",
    "last" : "https://www.example.com/collection/manifest/annotationPage.json",
    "type" : "AnnotationCollection",
    "id" : "https://www.example.com/annotationCollection/collection"
  }
}
```

#### Annotation Collection: Example for Manifest

`https://www.example.com/collection/manifest/annotationCollection.json`

```json
{
  "annotationCollection" : {
    "total" : 23,
    "@context" : "http://www.w3.org/ns/anno.jsonld",
    "first" : "https://www.example.com/collection/manifest/page1/annotationPage.json",
    "label" : "Annotations for some manifest.",
    "last" : "https://www.example.com/collection/manifest/page12/annotationPage.json",
    "type" : "AnnotationCollection",
    "id" : "https://www.example.com/annotationCollection/collection/manifest"
  }
}
```

#### Annotation Collection: Example for Item

`https://www.example.com/collection/manifest/page1/annotationCollection.json`

```json
{
  "annotationCollection" : {
    "total" : 2,
    "@context" : "http://www.w3.org/ns/anno.jsonld",
    "first" : "https://www.example.com/collection/manifest/page1/annotationPage.json",
    "label" : "Annotations for some manifest, page1",
    "type" : "AnnotationCollection",
    "id" : "https://www.example.com/annotationCollection/collection/manifest/page1"
  }
}
```

#### Annotation Page: Example for Manifest

`https://www.example.com/collection/manifest/annotationPage.json`

```json
{
  "annotationPage" : {
    "items" : [ {
        "body": {
          "type": "TextualBody",
          "value": "A person's name.",
          "format": "text/plain",
          "x-content-type": "Person"
        },
      "target" : {
        "format" : "text/xml",
        "language" : "eng",
        "selector" : {
          "type" : "CssSelector",
          "value" : "#MD775409N1l4l2l6l4l8l2"
        }
      },
      "type" : "Annotation",
      "id" : "https://www.example.com/annotationCollection/manifest/annotation-1"
    }, {
        "body": {
          "type": "TextualBody",
          "value": "The author probably means another person at this point.",
          "format": "text/plain",
          "x-content-type": "Editorial Comment"
        },
      "target" : {
        "format" : "text/xml",
        "language" : "eng",
        "selector" : {
          "type" : "CssSelector",
          "value" : "#MD775409N1l4l2l6l4l8l2"
        }
      },
      "type" : "Annotation",
      "id" : "https://www.example.com/annotationCollection/manifest/annotation-2"
    }],
    "partOf" : {
      "total" : 23,
      "label" : "Annotations for some manifest.",
      "id" : "https://www.example.com/annotationCollection/collection/manifest"
    },
    "@context" : "http://www.w3.org/ns/anno.jsonld",
    "startIndex" : 1,
    "prev" : null,
    "next" : "https://www.example.com/collection/manifest2/annotationPage.json",
    "id" : "https://www.example.com/annotationPage/collection/manifest"
  }
}
```

#### Annotation Page: Example for Item

`https://www.example.com/collection/manifest/page1/annotationPage.json`

```json
{
  "annotationPage" : {
    "items" : [ {
      "body": {
        "type": "TextualBody",
        "value": "This is a place in Azerbaijan.",
        "format": "text/plain",
        "x-content-type": "Person"
      },
      "target" : {
        "format" : "text/xml",
        "language" : "eng",
        "selector" : {
          "type" : "CssSelector",
          "value" : "#MD775409N1l4l2l6l4l8l2"
        }
      },
      "type" : "Annotation",
      "id" : "https://www.example.com/annotationCollection/manifest/annotation-1"
    }, {
      "body": {
        "type": "TextualBody",
        "value": "A person's name.",
      "format": "text/plain",
          "x-content-type": "Person"
      },
      "target" : {
        "format" : "text/xml",
        "language" : "eng",
        "selector" : {
          "type" : "CssSelector",
          "value" : "#MD775409N1l4l2l6l4l8l2"
        }
      },
      "type" : "Annotation",
      "id" : "https://www.example.com/annotationCollection/manifest/annotation-2"
    }],
    "partOf" : {
      "total" : 2,
      "label" : "Annotations for some manifest, page1.",
      "id" : "https://www.example.com/annotationCollection/collection/manifest/page1"
    },
    "@context" : "http://www.w3.org/ns/anno.jsonld",
    "startIndex" : 0,
    "prev" : null,
    "next" : "https://www.example.com/collection/manifest/page2/annotationPage.json",
    "type" : "AnnotationPage",
    "id" : "https://www.example.com/annotationPage/collection/manifest/page1"
  }
}
```
